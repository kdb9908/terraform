module "networkModule" {
    source			= "./modules/network"
 	access_key		= "${var.access_key}"
	secret_key		= "${var.secret_key}"
	region			= "${var.region}"
	environment_tag = "${var.environment_tag}"
}

module "securityModule" {
    source			= "./modules/security"
 	access_key		= "${var.access_key}"
	secret_key		= "${var.secret_key}"
	region			= "${var.region}"
	vpc_id			= "${module.networkModule.vpc_id}"
	environment_tag = "${var.environment_tag}"
}

module "instancesModule" {
	source 						= "./modules/instances"
	access_key 					= "${var.access_key}"
 	secret_key 					= "${var.secret_key}"
 	region     					= "${var.region}"
 	vpc_id 						= "${module.networkModule.vpc_id}"
	subnet_public_id			="${module.networkModule.public_subnets[0]}"
	subnet_private_id			="${module.networkModule.private_subnets[0]}"
	key_pair_name				="${module.networkModule.ec2keyName}"
	security_group_public_ids 	= ["${module.securityModule.sg_22}", "${module.securityModule.sg_80}", "${module.securityModule.sg_9997}"]
	security_group_private_ids 	= ["${module.securityModule.sg_3306}"]
	environment_tag 			= "${var.environment_tag}"
}


module "dnsModule" {
	source 		= "./modules/dns"
 	access_key 	= "${var.access_key}"
	secret_key 	= "${var.secret_key}"
	region     	= "${var.region}"
	domain_name	= "${var.domain_name}"
	aRecords	= [
		"${var.domain_name} ${module.instancesModule.instance_superveda_eip}",
		"splunk.${var.domain_name} ${module.instancesModule.instance_splunk_eip}",
		"rasp.${var.domain_name} ${module.instancesModule.instance_rasp_eip}",
		"acme.${var.domain_name} ${module.instancesModule.instance_acme_eip}"
	]
	cnameRecords	= [
		"www.${var.domain_name} ${var.domain_name}"
	]
}