output "superveda_server_ip" {
  value = "${module.instancesModule.instance_superveda_eip}"
}

output "splunk_server_ip" {
  value = "${module.instancesModule.instance_splunk_eip}"
}
output "rasp_manager_ip" {
  value = "${module.instancesModule.instance_rasp_eip}"
}
output "acme_server_ip" {
  value = "${module.instancesModule.instance_acme_eip}"
}
output "base_domain_nameservers" {  
  value = "${module.dnsModule.domain_name_servers}"
}