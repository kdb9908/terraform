output "sg_22" {
  value = "${aws_security_group.sg_22.id}"
}

output "sg_80" {
  value = "${aws_security_group.sg_80.id}"
}
output "sg_3306" {
  value = "${aws_security_group.sg_3306.id}"
}
output "sg_9997" {
  value = "${aws_security_group.sg_9997.id}"
}