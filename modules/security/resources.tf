provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "sg_22" {
  name = "sg_22"
  vpc_id = "${var.vpc_id}"
  description = "allow-ssh"

  # SSH access from the VPC
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_security_group" "sg_80" {
  name = "sg_80"
  vpc_id = "${var.vpc_id}"
  description = "allow-web"

  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_security_group" "sg_3306" {
  name = "sg_3306"
  vpc_id = "${var.vpc_id}"
  description = "allow-mysql"

  ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      security_groups = ["${aws_security_group.sg_80.id}"]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      self = true
  }
  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_security_group" "sg_9997" {
  name = "sg_9997"
  vpc_id = "${var.vpc_id}"
  description = "allow-splunk-forwarder"

  ingress {
      from_port = 9997
      to_port = 9997
      protocol = "tcp"
      cidr_blocks = ["10.1.0.0/24"]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      self = true
  }
  tags = {
    Environment = "${var.environment_tag}"
  }
}
