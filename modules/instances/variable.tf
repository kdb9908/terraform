# Variables

variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "eu-central-1"
}
variable "vpc_id" {
  description = "VPC id"
  default = ""
}
variable "subnet_public_id" {
  description = "VPC public subnet id"
  default = ""
}
variable "subnet_private_id" {
  description = "VPC private subnet id"
  default = ""
}
variable "security_group_public_ids" {
  description = "EC2 public security group"
  type = "list"
  default = []
}
variable "security_group_private_ids" {
  description = "EC2 private security group"
  type = "list"
  default = []
}
variable "environment_tag" {
  description = "Environment tag"
  default = ""
}
variable "key_pair_name" {
  description = "EC2 Key pair name"
  default = ""
}

variable "instance_type" {
  description = "EC2 instance type"
  default = "t2.micro"
}

variable "instance_username" {
  default = "ec2-user"
}