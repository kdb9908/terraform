output "instance_superveda_eip" {
  value = "${aws_eip.supervedaInstanceEip.public_ip}"
}

output "instance_acme_eip" {
  value = "${aws_eip.acmeInstanceEip.public_ip}"
}

output "instance_splunk_eip" {
  value = "${aws_eip.splunkInstanceEip.public_ip}"
}

output "instance_rasp_eip" {
  value = "${aws_eip.raspManagerInstanceEip.public_ip}"


}