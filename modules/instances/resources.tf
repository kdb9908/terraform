provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

data "aws_ami" "superveda-ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["Packer-Ansible-Superveda"]
  }                             
}

data "aws_ami" "acme-ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["Packer-Ansible-Acme"]
  }                             
}

data "aws_ami" "mysql-ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["Packer-Shell-MySql"]
  }                                
}

data "aws_ami" "splunk-ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["Packer-Shell-Splunk"]
  }                                
}

data "aws_ami" "raspManager-ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["Packer-Shell-RaspManager"]
  }                                
}

resource "aws_instance" "mysql" {
  ami           = "${data.aws_ami.mysql-ami.id}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet_private_id}"
  vpc_security_group_ids = "${var.security_group_private_ids}"
  key_name = "${var.key_pair_name}"

  tags = {
		Environment = "${var.environment_tag}"
    Name        = "MySql Server"
	}
  user_data = <<-EOF
		#! /bin/bash
    sudo service mysqld restart
	EOF
}

resource "aws_instance" "superveda" {
  ami           = "${data.aws_ami.superveda-ami.id}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet_public_id}"
  vpc_security_group_ids = "${var.security_group_public_ids}"
  key_name = "${var.key_pair_name}"

  tags = {
		Environment = "${var.environment_tag}"
    Name        = "Superveda Web Server"
	}

  user_data = <<-EOF
		#! /bin/bash
    #Set the mysql ip in the hosts file
		echo "${aws_instance.mysql.private_ip}  mysql" >> /etc/hosts
    #Set the splunk-indexer ip in the hosts file
		echo "${aws_instance.splunk.private_ip}  splunk" >> /etc/hosts
    #Restart Tomcat and Nginx
    sudo service tomcat8 restart
    sudo service nginx restart

	EOF
}

resource "aws_instance" "acme" {
  ami           = "${data.aws_ami.acme-ami.id}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet_public_id}"
  vpc_security_group_ids = "${var.security_group_public_ids}"
  key_name = "${var.key_pair_name}"

  tags = {
		Environment = "${var.environment_tag}"
    Name        = "Acme Web Server"
	}

  user_data = <<-EOF
		#! /bin/bash
    #Set the mysql ip in the hosts file
		echo "${aws_instance.mysql.private_ip}  mysql" >> /etc/hosts
    #Set the splunk-indexer ip in the hosts file
		echo "${aws_instance.splunk.private_ip}  splunk" >> /etc/hosts
    #Restart Tomcat and Nginx
    sudo service tomcat8 restart
    sudo service nginx restart

	EOF
}

resource "aws_instance" "splunk" {
  ami           = "${data.aws_ami.splunk-ami.id}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet_public_id}"
  vpc_security_group_ids = "${var.security_group_public_ids}"
  key_name = "${var.key_pair_name}"

  tags = {
		Environment = "${var.environment_tag}"
    Name        = "Splunk Server"
	}

  user_data = <<-EOF
		#! /bin/bash
    #Restart Splunk and Nginx
    sudo /opt/splunk/bin/splunk restart
    sudo service nginx restart
	EOF
}

resource "aws_instance" "raspManager" {
  ami           = "${data.aws_ami.raspManager-ami.id}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet_public_id}"
  vpc_security_group_ids = "${var.security_group_public_ids}"
  key_name = "${var.key_pair_name}"

  tags = {
		Environment = "${var.environment_tag}"
    Name        = "RASP Manager Server"
	}

  user_data = <<-EOF
		#! /bin/bash
    #Initializing and Starting Services
    sudo systemctl enable prevoty-manager
    sudo systemctl start prevoty-manager
    sudo systemctl enable prevoty-trustedquery
    sudo systemctl start prevoty-trustedquery
    #Restart Nginx
    sudo service nginx restart
	EOF
}


resource "aws_eip" "supervedaInstanceEip" {
  vpc       = true
  instance  = "${aws_instance.superveda.id}"

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_eip" "acmeInstanceEip" {
  vpc       = true
  instance  = "${aws_instance.acme.id}"

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_eip" "splunkInstanceEip" {
  vpc       = true
  instance  = "${aws_instance.splunk.id}"

  tags = {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_eip" "raspManagerInstanceEip" {
  vpc       = true
  instance  = "${aws_instance.raspManager.id}"

  tags = {
    Environment = "${var.environment_tag}"
  }
}


