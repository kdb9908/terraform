variable "access_key" { 
  default = ""
}
variable "secret_key" { 
  default = ""
}
variable "region" {
  default = "eu-central-1"
}
variable "availability_zone" {
  default = "eu-central-1b"
}
variable "environment_tag" {
  description = "Environment tag"
  default = "DevSecOps Lab"
}

variable "domain_name" {
  description = "Domain Name"
  default = "superveda.fr"
}